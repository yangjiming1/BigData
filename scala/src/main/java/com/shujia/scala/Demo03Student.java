package com.shujia.scala;

public class Demo03Student {
    private String id;
    private String name;
    private int age;

    @Override
    public String toString() {
        return "Demo3Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Demo03Student() {
    }

    public Demo03Student(String id, String name, int age) {

        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
