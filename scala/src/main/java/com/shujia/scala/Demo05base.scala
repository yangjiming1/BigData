package com.shujia.scala

/**
 * @author yangjiming
 * @create 2021-05-06 23:29
 */
object Demo05base {
  def main(args: Array[String]): Unit = {
    var a=300
    a=400
    println(a)

    val str1 = "java"

    val str2:String = "java"

    //类型转换
    val s = "100"
    val si:Int = Integer.parseInt(s)

    println(si)

    println("="*20)
    val sj = s.toInt
    println(sj)
  }

}
