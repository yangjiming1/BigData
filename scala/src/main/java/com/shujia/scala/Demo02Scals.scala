package com.shujia.scala

/**
 * @author yangjiming
 * @create 2021-05-06 12:17
 */
object Demo02Scals {
  def main(args: Array[String]): Unit = {
    println("hello scala")

    var student = new Demo03Student("1001","zhangsan",18)
    println(student)

    val s:String = "scala"
    val split = s.split(",")
    println(split)
    val a = 100
    val b = 200
    println(s"a=${a},b=$b,a+b=${a+b}")
  }
}
