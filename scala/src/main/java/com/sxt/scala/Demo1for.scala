package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-11 19:44
 */
object Demo1for {
  def main(args: Array[String]): Unit = {
    println(1 to 10)
    println(1 until 10)

    for (i <- 1 to 10) {
      println(s"i = ${i}")
    }

    for (i <- 1 to(10, 4)) {
      println(s"i = ${i}")
    }


    //for循环嵌套
    for (i <- 1 to 10) {
      for (j <- 1 to 10) {
        //println(s"i = ${i},j = $j")
      }
    }
    println()
    // for 循环嵌套也可以这样写
    for (i <- 1 to 10; j <- 1 to 10) {

      println(s"i = ${i},j = $j")
    }

    // for 加条件
    for ( i<-1 to 100 if (i%2==0)){
      println(i)
    }

  }
}
