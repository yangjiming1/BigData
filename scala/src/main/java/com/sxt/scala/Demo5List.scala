package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-12 10:24
 */
object Demo5List {
  def main(args: Array[String]): Unit = {
    val list = List[String]("abc abc", "def def", "123 123")
    val listArrs: List[Array[String]] = list.map(str => {
      str.split(" ")
    })

    val listFlar: List[String] = list.flatMap(f => {
      f.split(" ")
    })
    listArrs.foreach(arr => {
      println("############")
      arr.foreach(println)
    })

    listFlar.foreach(
      println
    )
  }

}
