package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-11 20:38
 */
object Demo3Fun {
  def main(args: Array[String]): Unit = {

    def max(x: Int, y: Int): Int = {
      if (x > y) {
        x
      }
      else
        y
    }


    val i: Int = max(18, 4)
    println(i)


    // 参数可以设置默认值
    def fun(a: Int = 10, b: Int = 20): Int = {
      a + b
    }

    println(fun())
    println(fun(5))
    println(fun(b = 5))
    println(fun(5, 6))


    // 不定长参数的方法
    def fun1(s: String*): Unit = {
      for (elem <- s) {
        println(elem)
      }
    }

    fun1("hello", "world")


    /**
     * 高阶函数
     */

    // 方法的参数是函数
    def fun2(a: Int, b: Int): Int = {
      a + b
    }

    def fun3(s: String, f: (Int, Int) => Int): String = {
      val i1: Int = f(10, 20)
      s + "@" + i1
    }

    println(fun3("hello", fun2))

    // 方法的返回值是函数
    def fun4(a: Int, b: Int): (String, String) => String = {
      val i1: Int = a + b

      def fun5(s1: String, s2: String): String = {
        s1 + "#" + s2 + "@" + i1
      }

      fun5
    }

    println(fun4(100, 200)("hello", "world"))

    // 参数和返回值都有函数
    def fun6(a:Int,b:String,f:(Int,Int)=>Int):(String,String)=>String={
      val str: String = a + "#" + b + "~" + f(10, 20)
    def fun7(s1:String,s2:String  ):String={
        s1+s2+str
    }
      fun7
    }

    println(fun6(100, "hello", (a: Int, b: Int) => {
      a + b
    })("a", "b"))

  }
}
