package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-12 10:47
 */
object Demo6Set {
  def main(args: Array[String]): Unit = {
    val set: Set[Int] = Set[Int](1, 2, 3, 4, 5, 5, 6)
    // 无序不可重复
    set.foreach(println)
  }

}
