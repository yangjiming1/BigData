package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-11 20:28
 */
object Demo2while {
  def main(args: Array[String]): Unit = {
    var i = 1
    while (i < 100) {
      println(s"第${i}次求婚")
      i += 1
    }

    println("完蛋了，你小子别结婚了")
    i = 1
    do {
      println(s"第${i}次求婚")
      i += 1
    } while (i < 100)

  }

}
