package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-12 11:16
 */
object Demo7Map {
  def main(args: Array[String]): Unit = {
    val map: Map[Int, String] = Map[Int, String](1 -> "zhangsan", 2 -> "lisi", (3, "wangwu"))
    map.foreach(println)
    val str: String = map.get(10).getOrElse("qita")
    val str1: String = map.getOrElse(3, "sss")
    println(str)
    println(str1)

    val keys: Iterable[Int] = map.keys
    println(keys)
    keys.foreach(println)
  }
}
