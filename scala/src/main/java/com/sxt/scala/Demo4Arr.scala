package com.sxt.scala

/**
 * @author yangjiming
 * @create 2021-05-12 9:40
 */
object Demo4Arr {
  def main(args: Array[String]): Unit = {
      // 1.定义
    val arr = Array[String]("a","b","c")
    val arr1 = new Array[String](4)
    arr1(0)="a"
    arr1(1)="b"
    arr1(2)="c"
    arr1(3)="d"
    // 2.遍历
    for (elem <- arr) {
      println(elem)
    }

    arr1.foreach(println)
  }
  println()
  // 二维数组
  val array = new Array[Array[String]](3)
  array(0)=Array("a","b","c")
  array(1)=Array("1","2","3")
  array(2)=Array("d","e","f")

  array.foreach(arr2=>{
    arr2.foreach(println)
  })


}
