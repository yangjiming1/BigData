package com.shujia;

import java.sql.*;

/**
 * @author yangjiming
 * @create 2021-04-14 19:56
 */
public class hiveJDBCTest {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //创建链接
        Connection conn = DriverManager.getConnection("jdbc:hive2://master:10000/test1");
        //加载Hive驱动
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        //创建statement
        Statement stat = conn.createStatement();
        //执行SQL
        ResultSet rs = stat.executeQuery("select * from students_dt_p where dt='20210101'");
        while(rs.next()){
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int age = rs.getInt(3);
            String gender = rs.getString(4);
            String clazz = rs.getString(5);
            System.out.println(id + "," + name + "," + age + "," + gender + "," + clazz);
        }
        rs.close();
        stat.close();
        conn.close();
    }
}
