package com.shujia.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {

    static Connection con;

    static {


        //2、验证用户名是否存在
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //创建连接
        try {
            con = DriverManager.getConnection("jdbc:mysql://master:3306/user", "root", "123456");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * 创建连接的工具
     */

    public static Connection getConnection() {
        return con;
    }
}
