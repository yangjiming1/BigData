package com.shujia.mvc;

import com.shujia.util.MD5;

/**
 * @author yangjiming
 * @create 2021-03-30 21:28
 */
public class UserService {
    UserDao userDao = new UserDao();

    public String login(String username, String password) {
        User user = userDao.findUserByUsername(username);

        if (user == null) {
            return "用户名不正确";
        }
        String md5Password = MD5.md5(password);
        if (!md5Password.equals(user.getPassword())) {
            return "密码错误";
        }

        return "登录成功";
    }

    public String register(String username, String password, String newPassword) {

        User user = userDao.findUserByUsername(username);
        //判断用户名是否存在
        if(user!=null){
            return "用户名存在";
        }
        //判断密码是否一致
        if(!password.equals(newPassword)){
            return "密码不一致";
        }
        //进行密码加密，存入数据库
        String md5Password = MD5.md5(password);
        int insert = userDao.insert(username, md5Password);

        if(insert==0){
            return "注册失败";
        }

        return "注册成功";
    }

    public String modifyPassword(String username,String password,String newPassword,String nowPassword){
        User user = userDao.findUserByUsername(username);

        //判断用户名是否存在
        if(user==null){
            return "用户名不存在";
        }

        //判断原密码是否正确
        if(!MD5.md5(password).equals(user.getPassword())){
            return "密码错误";
        }

        if(!newPassword.equals(nowPassword)){
            return "密码不一致";
        }

        int i = userDao.upadtePasswordByUsername(username, MD5.md5(newPassword));
        if(i==0){
            return "修改失败";
        }
        return "修改成功";
    }


}
