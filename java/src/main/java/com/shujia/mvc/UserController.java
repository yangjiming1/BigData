package com.shujia.mvc;

import org.junit.Test;

import java.util.Scanner;

/**
 * @author yangjiming
 * @create 2021-03-30 21:29
 */
public class UserController {
    @Test
    public void login() {
        UserService userService = new UserService();
        Scanner scan = new Scanner(System.in);

        System.out.println("请输入用户名：");
        String username = scan.next();

        System.out.println("请输入密码：");
        String password = scan.next();

        String login = userService.login(username, password);

        System.out.println(login);


    }

    @Test
    public void register() {
        UserService userService = new UserService();
        Scanner scan = new Scanner(System.in);

        System.out.print("请输入用户名：");
        String username = scan.next();

        System.out.print("请输入密码：");
        String password = scan.next();

        System.out.print("请确认密码：");
        String newPassword = scan.next();

        String register = userService.register(username, password, newPassword);
        System.out.println(register);
    }

    @Test
    public void modifyPassword(){
        UserService userService = new UserService();
        Scanner scan = new Scanner(System.in);

        System.out.print("请输入用户名：");
        String username = scan.next();

        System.out.print("请输入原密码：");
        String password = scan.next();

        System.out.print("请输入新密码：");
        String newPassword = scan.next();

        System.out.print("请确认新密码：");
        String nowPassword = scan.next();

        String s = userService.modifyPassword(username, password, newPassword, nowPassword);
        System.out.println(s);


    }


}
