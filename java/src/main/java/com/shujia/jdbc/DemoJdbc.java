package com.shujia.jdbc;

import java.sql.*;

/**
 * @author yangjiming
 * @create 2021-03-30 19:54
 */
public class DemoJdbc {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://master:3306/student", "root", "123456");
            PreparedStatement ps = conn.prepareStatement("select * from student");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String gender = rs.getString("gender");
                String clazz = rs.getString("clazz");

                System.out.println(id + "\t" + name + "\t" + age + "\t" + gender + "\t" + clazz);
            }

            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
