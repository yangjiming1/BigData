package com.shujia.user;

import java.sql.*;
import java.util.Scanner;

/**
 * @author yangjiming
 * @create 2021-03-30 20:49
 */
public class DemoLogin {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("请输入用户名：");
        String username = scan.next();

        System.out.println("请输入密码：");
        String password = scan.next();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://master:3306/user", "root", "123456");

            PreparedStatement ps = conn.prepareStatement("select * from t_user where username= ? ");
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();

            if(!rs.next()){
                System.out.println("用户名不存在。。");
                return ;
            }
            String password1 = rs.getString("password");
            if(!password.equals(password1)){
                System.out.println("密码错误。。");
                return ;
            }

            System.out.println("登录成功");

            conn.close();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
}
