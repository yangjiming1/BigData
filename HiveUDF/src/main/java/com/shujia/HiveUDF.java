package com.shujia;

import org.apache.hadoop.hive.ql.exec.UDF;


/**
 * @author yangjiming
 * @create 2021-04-16 16:58
 */
public class HiveUDF extends UDF {
    public String evaluate(String input){
        return "#"+input+"&";
    }
}
