package com.shujia;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * @author yangjiming
 * @create 2021-04-21 22:11
 */
public class Demo01Test {
    public static void main(String[] args) {
        //创建配置
        Configuration conf = HBaseConfiguration.create();
        //设置hbase zk连接地址
        conf.set("hbase.zookeeper.quorum", "master:2181,node1:2181,node2:2181");

        //创建连接
        try {
            Connection conn = ConnectionFactory.createConnection(conf);
            Admin admin = conn.getAdmin();

            // 构建NamespaceDescriptor对象
            NamespaceDescriptor apiNS = NamespaceDescriptor.create("apiNS").build();
            admin.createNamespace(apiNS);

            // 创建HTableDescriptor对象，设置表的name
            HTableDescriptor test1 = new HTableDescriptor(TableName.valueOf("apiNS:test1"));
            // 对表做一些配置
            // test1.setMemStoreFlushSize(128);
            // HColumnDescriptor对象，设置列簇的name
            HColumnDescriptor cf1 = new HColumnDescriptor("cf1");
            // 对列簇做一些配置
            cf1.setTimeToLive(500);
            cf1.setMaxVersions(5);
            //将列簇添加到表中
            test1.addFamily(cf1);

            HColumnDescriptor cf2 = new HColumnDescriptor("cf2");
            test1.addFamily(cf2);
            // 表配置好了，列簇配置好了，使用admin的createTable方法，传入HTableDescriptor对象，完成表的创建

            admin.createTable(test1);
            //关闭连接
            conn.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
