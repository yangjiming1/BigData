package com.shujia;

import java.sql.*;

/**
 * @author yangjiming
 * @create 2021-04-25 9:59
 */
public class Demo08PhoenixJDBC {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:phoenix:master,node1,node2:2181");
        String sql = "select end_date,MDN,COUNTY from DIANXIN where end_date = ? and COUNTY = ?";
        PreparedStatement pStat = conn.prepareStatement(sql);
        pStat.setString(1, "20180503154014");
        pStat.setString(2, "8340104");

        ResultSet rs = pStat.executeQuery();
        while (rs.next()) {
            String end_date = rs.getString("end_date");
            String MDN = rs.getString("MDN");
            String COUNTY = rs.getString("COUNTY");
            System.out.println(end_date + "\t" + MDN + "\t" + COUNTY);
        }

        conn.close();

    }
}
