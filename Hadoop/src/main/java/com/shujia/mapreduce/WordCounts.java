package com.shujia.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class WordCounts {
    //map过程
    //创建静态类继承Mapper给上输入输出形参
    public static class WordCountMap extends Mapper<LongWritable,Text,Text,LongWritable>{
        @Override  //重写map方法
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            //每一次获取一行数据
            String line = value.toString();//通过values获取一行数据
            //切分字符串
            String[] split = line.split(",");
            for (String s : split) {
                context.write(new Text(s),new LongWritable(1));
            }

        }
    }
    //reduce过程
    public static class WordCountReduce extends Reducer<Text,LongWritable,Text,LongWritable>{
        @Override       //map中的多个数据同时来到reduce,在Iterable<LongWritable> values存储多个数据
        protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
            Long sum=0l;//单词统计的结果
            //对Iterable<LongWritable> values进行增强for获取一个一个kv键值对
            //reduce之前shuffle过程把map中相同的k存储到同一个reduce中
            //聚合不需要判断直接把直接把单词个数进行累加
            for (LongWritable value : values) {
                sum=sum+value.get();
            }
            //输出结果<Text,LongWritable>获取Text当前单词
            String line = key.toString();
            //把结果输出到hdfs
            context.write(new Text(line),new LongWritable(sum));
        }
    }
    //启动MapReduce,并且连接这两个独立的任务
    public static void main(String[] args) throws Exception{
        Job job = Job.getInstance();//构建一个job
        //指定job任务名称
        job.setJobName("wordcounts");
        //指定当前main所在类名
        job.setJarByClass(WordCounts.class);

        //指定map类
        job.setMapperClass(WordCountMap.class);
        //指定map输出k和v的类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        //指定reduce
        job.setReducerClass(WordCountReduce.class);
        //指定reduce输出k和v的类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        //指定输入路径
        FileInputFormat.addInputPath(job,new Path("/data/words.txt"));
        FileSystem fs = FileSystem.get(new Configuration());
        Path path = new Path("/output");
        //指定输出路径(路径不能已存在),如果已存在删除
        if(fs.exists(path)){
            fs.delete(path,true);
            System.out.println("源文件存在，删除");
        }
        //指定输出路径(路径不能已存在)
        FileOutputFormat.setOutputPath(job,path);
        //启动
        job.waitForCompletion(true);

    }
}
