package com.shujia.spark

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * @author yangjiming
 * @create 2021-05-11 11:18
 */
object Demo03Map {
  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("map")
    val sc = new SparkContext(conf)

    val rdd1: RDD[Int] = sc.parallelize(List(1, 2, 3, 4, 5, 6, 7, 8, 9))
    val rdd2: RDD[String] = rdd1.map((i: Int) => {
      println(s"map${i}")
      i.toString
    })

    rdd2.foreach(println)
  }

}
