package com.shujia.redis;

import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class RedisApi {
    Jedis jedis = null;

    @Before
    public void init() {
        //1、创建连接
        jedis = new Jedis("master", 6379);
    }

    @Test
    public void get() {


        //jedis.set("java","shujia");
        //2、查询数据
        String java = jedis.get("java");

        System.out.println(java);

        //关闭连接
        jedis.close();

    }

    @Test
    public void student() throws Exception{
        FileReader fileReader = new FileReader("data/students.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        while((line=bufferedReader.readLine())!=null){

            String id = "student:" + line.split(",")[0];

            String[] split = line.split(",");
            String name = split[1];
            String age = split[2];
            String gender = split[3];
            String clazz = split[4];

            HashMap<String, String> hashMap = new HashMap<>();

            hashMap.put("name",name);
            hashMap.put("age",age);
            hashMap.put("gender",gender);
            hashMap.put("clazz",clazz);

            jedis.hmset(id,hashMap);

        }

    }

    @Test
    public void hget(){
//        String hget = jedis.hget("student:1500100002", "name");
//        System.out.println(hget);
        Map<String, String> map = jedis.hgetAll("student:1500100002");

        System.out.println(map);

    }
}
