package com.shujia.dao;

import com.shujia.bean.Student;
import com.shujia.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yangjiming
 * @ClassName StudentDao
 * @Description
 * @create 2021-03-31 20:27
 */
public class StudentDao {

    public Student queryStudentById(Integer id){
        Connection conn = JDBCUtil.getConnection();
        Student student = new Student();

        try {
            PreparedStatement ps = conn.prepareStatement("select * from student where id =?");
            ps.setInt(1,id);

            //执行查询
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                String name = rs.getString("name");
                Integer age = rs.getInt("age");
                String gender = rs.getString("gender");
                String clazz = rs.getString("clazz");


                student.setId(id);
                student.setName(name);
                student.setAge(age);
                student.setGender(gender);
                student.setClazz(clazz);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return student;
    }

}
