package com.shujia.dao;


import com.mysql.jdbc.PreparedStatement;
import com.shujia.bean.Weibo;
import com.shujia.util.JDBCUtil;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yangjiming
 * @ClassName WeiboDao
 * @Description
 * @create 2021-04-02 11:15
 */

@Service
public class WeiboDao {
    //查询数据库
    public Weibo queryWeiboById(Integer id){
        //获取数据库连接
        Connection conn = JDBCUtil.getConnection();
        Weibo weibo = null;
        try {
            java.sql.PreparedStatement ps = conn.prepareStatement("select * from t_weibo where id = ?");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                weibo = new Weibo();
                weibo.setId(id);
                weibo.setText(rs.getString("text"));

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {

            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return weibo;
    }
}
