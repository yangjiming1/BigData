package com.shujia.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class MakeStudentData {
    public static void main(String[] args) throws Exception {


        FileReader fileReader = new FileReader("spring/data/students.txt");

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        //创建连接
        Connection conn = JDBCUtil.getConnection();
        PreparedStatement ps = null;
        int count = 0;
        long start = System.currentTimeMillis();
        while ((line = bufferedReader.readLine()) != null) {

            String[] split = line.split(",");
            //Integer id = Integer.parseInt(split[0]);

            String name = split[1];
            int age = Integer.parseInt(split[2]);

            String gender = split[3];
            String clazz = split[4];

            ps = conn.prepareStatement("insert into student(`name`,age,gender,clazz) values (?,?,?,?)");

            ps.setString(1,name);
            ps.setInt(2,age);
            ps.setString(3,gender);
            ps.setString(4,clazz);

            for(int i=0;i<1000;i++){
                ps.executeUpdate();
            }
            count++;
            System.out.println("插入第"+count+"个1000条数据成功");

        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        conn.close();


    }
}
