package com.shujia.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

    /**
     * 对字符串进行md5加密
     *
     * @param plainText 字符串
     * @return md5字符串
     */
    public static String md5(String plainText) {

        String s = plainText + "shujia";

        byte[] secretBytes;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(s.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        StringBuilder md5code = new StringBuilder(new BigInteger(1, secretBytes).toString(16));
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code.insert(0, "0");
        }
        return md5code.toString().toUpperCase();
    }

    public static void main(String[] args) {

        String s = md5("123456");
        System.out.println(s);

    }
}
