package com.shujia.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {


    static JedisPool jedisPool;

    static {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMinIdle(2);
        //获取连接的超时时间
        jedisPoolConfig.setMaxWaitMillis(100);

        //使用连接池
        jedisPool = new JedisPool(jedisPoolConfig, "master", 6379);


    }

    public static Jedis getConnection() {

        return jedisPool.getResource();
    }
}
