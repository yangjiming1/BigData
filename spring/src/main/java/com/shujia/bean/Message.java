package com.shujia.bean;

/**
 * @author yangjiming
 * @ClassName Message
 * @Description
 * @create 2021-03-31 17:00
 */
public class Message {
    private Integer code;
    private String msg;

    public Message() {
    }

    public Message(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
