package com.shujia.bean;

import lombok.*;

/**
 * @author yangjiming
 * @ClassName Weibo
 * @Description
 * @create 2021-04-02 11:15
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Weibo {
    private Integer id;
    private String text;
}
