package com.shujia.test;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;

public class Demo2C3p0 {
    public static void main(String[] args) throws Exception {

        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://node3:3306/shujia");
        dataSource.setUser("root");
        dataSource.setPassword("123456");


        dataSource.setInitialPoolSize(2);
        dataSource.setMaxPoolSize(10);


        //获取连接
        Connection con = dataSource.getConnection();


        con.close();
    }
}
