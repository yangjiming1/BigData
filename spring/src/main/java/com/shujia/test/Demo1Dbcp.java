package com.shujia.test;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Demo1Dbcp {

    public static void main(String[] args) throws Exception {

        //创建连接池

        BasicDataSource dataSource = new BasicDataSource();

        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://node3:3306:/shujia");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");

        //初始连接池大小
        dataSource.setInitialSize(2);

        //最大连接数量
        dataSource.setMaxIdle(10);

        //从连接池中获取连接
        //再连接池中已经创建好很多连接了，每一次获取连接是不重新创建

        Connection con = dataSource.getConnection();

        PreparedStatement stat = con.prepareStatement("select * from student");
        ResultSet resultSet = stat.executeQuery();


        //关闭连接, 相当于返回到连接池
        con.close();


    }
}
