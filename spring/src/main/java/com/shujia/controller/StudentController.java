package com.shujia.controller;

import com.shujia.bean.Student;
import com.shujia.service.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangjiming
 * @ClassName StudentController
 * @Description
 * @create 2021-03-31 20:27
 */
@RestController
public class StudentController {
    private StudentService studentService = new StudentService();
    @GetMapping("/queryStudentById")
    public Student queryStudentById(Integer id){
        long start = System.currentTimeMillis();

        Student student = studentService.queryStudentById(id);

        long end = System.currentTimeMillis();

        System.out.println(end - start);

        return student;

    }
}
