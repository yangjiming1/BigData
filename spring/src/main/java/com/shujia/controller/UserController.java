package com.shujia.controller;

import com.shujia.bean.Message;
import com.shujia.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangjiming
 * @create 2021-03-30 21:29
 */
@RestController
public class UserController {
    @GetMapping("/login")
    public Message login(String username, String password) {
        UserService userService = new UserService();

        Message login = userService.login(username, password);
        return login;
    }

    @GetMapping("/register")
    public Message register(String username,String password,String newPassword) {
        UserService userService = new UserService();

        Message register = userService.register(username, password, newPassword);
        return register;
    }

    @GetMapping("/modifyPassword")
    public Message modifyPassword(String username,String password,String newPassword,String nowPassword){
        UserService userService = new UserService();

        Message s = userService.modifyPassword(username, password, newPassword, nowPassword);
        return s;
    }
}
