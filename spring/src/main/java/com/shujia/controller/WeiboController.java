package com.shujia.controller;

import com.shujia.bean.Weibo;
import com.shujia.service.WeiboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangjiming
 * @ClassName WeiboController
 * @Description
 * @create 2021-04-02 11:15
 */

@RestController
public class WeiboController {
    @Autowired
    WeiboService weiboService;

    @GetMapping("/queryWeibo")
    public Weibo queryWeibo(Integer id){
        Weibo weibo = weiboService.queryWeiboById(id);
        return weibo;
    }
}
