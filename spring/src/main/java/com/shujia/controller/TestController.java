package com.shujia.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yangjiming
 * @ClassName TestController
 * @Description
 * @create 2021-03-31 15:44
 */

@RestController
public class TestController {
    @GetMapping("/test")
    public String test(String world){
        System.out.println(world);
        return "测试spring";
    }

}
