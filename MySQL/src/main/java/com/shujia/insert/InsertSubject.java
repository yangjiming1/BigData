package com.shujia.insert;

import com.shujia.util.JDBCUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author yangjiming
 * @create 2021-04-12 23:21
 */
public class InsertSubject {
    public static void main(String[] args) throws Exception {


        FileReader fileReader = new FileReader("MySQL/data/subject.txt");

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        //获取连接
        Connection connection = JDBCUtil.getConnection();

        while ((line = bufferedReader.readLine()) != null) {
//1000009,生物
            String[] split = line.split(",");
            int score_id = Integer.parseInt(split[0]);

            String sub_name = split[1];

            //for (int i = 0; i < 1000; i++) {


            PreparedStatement stat = connection.prepareStatement("insert into subject values(?,?)");

            stat.setInt(1, score_id);
            stat.setString(2, sub_name);

            stat.execute();

//            }
//            System.out.println("插入1000条成功");

        }


    }
}
