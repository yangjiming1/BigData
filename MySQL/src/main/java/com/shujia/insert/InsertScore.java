package com.shujia.insert;

import com.shujia.util.JDBCUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author yangjiming
 * @create 2021-04-12 23:20
 */
public class InsertScore {
    public static void main(String[] args) throws Exception {


        FileReader fileReader = new FileReader("MySQL/data/score.txt");

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        //获取连接
        Connection connection = JDBCUtil.getConnection();

        while ((line = bufferedReader.readLine()) != null) {
           // 1500100001,1000001,98
            String[] split = line.split(",");
            int id = Integer.parseInt(split[0]);

            int score_id = Integer.parseInt(split[1]);
            int score = Integer.parseInt(split[2]);

            //for (int i = 0; i < 1000; i++) {


            PreparedStatement stat = connection.prepareStatement("insert into score values(?,?,?)");

            stat.setInt(1, id);
            stat.setInt(2, score_id);
            stat.setInt(3, score);


            stat.execute();

//            }
//            System.out.println("插入1000条成功");

        }


    }

}
