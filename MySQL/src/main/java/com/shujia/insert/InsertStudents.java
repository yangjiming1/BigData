package com.shujia.insert;

import com.shujia.util.JDBCUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author yangjiming
 * @create 2021-04-12 21:44
 */
public class InsertStudents {
    public static void main(String[] args) throws Exception {


        FileReader fileReader = new FileReader("MySQL/data/students.txt");

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        //获取连接
        Connection connection = JDBCUtil.getConnection();

        while ((line = bufferedReader.readLine()) != null) {
//1500100005,宣谷芹,22,女,理科五班
            String[] split = line.split(",");
            int id = Integer.parseInt(split[0]);

            String name = split[1];
            int age = Integer.parseInt(split[2]);

            String gender = split[3];
            String clazz = split[4];

            //for (int i = 0; i < 1000; i++) {


                PreparedStatement stat = connection.prepareStatement("insert into students(id,name,age,gender,clazz) values(?,?,?,?,?)");

                stat.setInt(1, id);
                stat.setString(2, name);
                stat.setInt(3, age);
                stat.setString(4, gender);
                stat.setString(5, clazz);


                stat.execute();

//            }
//            System.out.println("插入1000条成功");

        }


    }
}
